//Reverse a string
function reverseString(str) {
    return str.split("").reverse().join("")
}
let str = reverseString("hello")
console.log(str)
console.assert(reverseString("test") === "tset");
console.assert(reverseString("pink") === "knip");


//Reverse a sentence ("bob likes dogs" -> "dogs likes bob")
function reverseSent(sent) {
    return sent.split(" ").reverse().join(" ")
}

let sent = reverseSent("bob likes dogs")
console.log(sent)
console.assert(reverseSent("bob likes dogs") === "dogs likes bob");


//Find the minimum value in an arr
const sumSmall = [469, 755, 244]

function findMin(arr) {
    let minValue = arr[0]
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] < minValue) {
            minValue = arr[i]
        }
    }
    return minValue
}

let  minOfArray= findMin(sumSmall)
console.log(minOfArray)
console.assert(findMin(sumSmall) === 244);


//Find the maximum value in an arr

const largeSum = [469, 755, 244]

function findTheMax(arr) {
    const maxValue = arr[0]
    for (const i = 0; i < arr.length; i++) {
        if (arr[i] > maxValue) {
            maxValue = arr[i]
        }
    }
    return maxValue
}

let newMax = findTheMax(sumLarge)
console.log(newMax)
console.assert(findTheMax(sumLarge) === 755);

//Calculate a remainder (given a numerator and denominator)

function findRem(denominator, numerator) {
    return numerator % denominator
}
console.log(findRem(4, 2));
console.assert(findRem(4, 2) === 2);

//Return distinct values from a list including duplicate (i.e. "1 3 5 3 7 3 1 1 5" -> "1 3 5 7")

const dupValues = [1, 3, 5, 3, 7, 3, 1, 1]

function duplicate(arr) {
    let uniArray = []
    for (const i = 0; i < arr.length; i++) {
        if (!uniArray.includes(arr[i])) {
            uniArray.push(arr[i])
        }

    }
    return uniArray
}

console.log(duplicate(dupValues))

console.assert(JSON.stringify(duplicate(dupValues)) === JSON.stringify([1, 3, 5, 7]))

JSON.stringify()


 //Return distinct values and their counts (i.e. the list above becomes "1(3) 3(3) 5(2) 7(1)")

 const countValues = [1, 3, 5, 3, 7, 3, 1, 1]

function count(arr) {
    let countValue = {}
    let finString = ''
    for (const i = 0; i < arr.length; i++) {
        if (!(arr[i] in countValue)) {
           countValue[arr[i]] = 1
        } else {
            countValue[arr[i]]++
        } 

    }

    for (i in countValue){
        finString += i+'('+countValue[i]+')'+' '
    }
    return finString
}

console.log(count(countValues))

console.assert(count(countValues) === ('1(3) 3(3) 5(1) 7(1) '))


 //Given a string of expressions (only variables, +, and -) and an object describing a set of variable/value pairs like {a: 1, b: 7, c: 3, d: 14}, return the result of the expression ("a + b+c -d" would be -3).


function yes(eq, object) {
  let a = eq.split(" ")
  for (let i = 0; i < a.length; i++) {
      if (a[i] === '+' || a[i] === '-') {
          a[i] = a[i]
      } else {
          a[i] = "object." + a[i]
      }
  }
  a = a.join(" ")
  let b = a.toString()
  console.log(b)
  console.log(eval(b))
  return eval(b)
}
console.log("8=" + yes("a + b + c - d", { a: 1, b: 7, c: 3, d: 14 }))
console.log("8=" + yes("a + b + c - d + e", { a: 1, b: 7, c: 3, d: 14, e: 3 }))
console.assert(yes("a + b + c - d", { a: 1, b: 7, c: 3, d: 14 }) === -3)
